<?php if($allsettings->maintenance_mode == 0): ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<title><?php echo e($allsettings->site_title); ?> - <?php echo e(__('Contact')); ?></title>
<?php echo $__env->make('meta', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('style', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php if($additional->site_google_recaptcha == 1): ?>
<?php echo RecaptchaV3::initJs(); ?>

<?php endif; ?>
</head>
<body>
<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<section class="bg-position-center-top" style="background-image: url('<?php echo e(url('/')); ?>/public/storage/settings/<?php echo e($allsettings->site_banner); ?>');">
      <div class="py-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
        <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb flex-lg-nowrap justify-content-center justify-content-lg-star">
              <li class="breadcrumb-item"><a class="text-nowrap" href="<?php echo e(URL::to('/')); ?>"><i class="dwg-home"></i><?php echo e(__('Home')); ?></a></li>
              <li class="breadcrumb-item text-nowrap active" aria-current="page"><?php echo e(__('Contact')); ?></li>
            </ol>
          </nav>
        </div>
        <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
          <h1 class="h3 mb-0 text-white"><?php echo e(__('Contact')); ?></h1>
        </div>
      </div>
      </div>
    </section>
   <!-- Outlet stores-->
    <div class="container px-0" id="map">
      <?php if(in_array('contact',$top_ads)): ?>
      <div class="row">
          <div class="col-lg-12 mt-4" align="center">
             <?php echo html_entity_decode($addition_settings->top_ads); ?>
          </div>
       </div>   
       <?php endif; ?>
      <div class="row">
        <div class="col-lg-5 px-4 px-xl-5 py-5">
          <form method="POST" action="<?php echo e(route('contact')); ?>" id="contact_form"  class="needs-validation mb-3" novalidate>
          <?php echo csrf_field(); ?>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="cf-name"><?php echo e(__('Name')); ?> <span class="text-danger">*</span></label>
                  <input class="form-control" type="text" id="from_name" name="from_name" data-bvalidator="required">
                  <div class="invalid-feedback"><?php echo e(__('Please fill in you full name')); ?></div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="cf-email"><?php echo e(__('Email')); ?> <span class="text-danger">*</span></label>
                  <input class="form-control" type="text" id="cf-email" name="from_email" data-bvalidator="email,required">
                  <div class="invalid-feedback"><?php echo e(__('Please provide valid email address')); ?></div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="cf-message"><?php echo e(__('Message')); ?> <span class="text-danger">*</span></label>
              <textarea class="form-control" id="cf-message" rows="6" name="message_text" data-bvalidator="required"></textarea>
              <div class="invalid-feedback"><?php echo e(__('Please write a message')); ?></div>
            </div>
            <?php if($additional->site_google_recaptcha == 1): ?>
            <div class="form-group<?php echo e($errors->has('g-recaptcha-response') ? ' has-error' : ''); ?>">
                            <?php echo RecaptchaV3::field('register'); ?>

                                <?php if($errors->has('g-recaptcha-response')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('g-recaptcha-response')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>
            <?php endif; ?>
            <button class="btn btn-primary" type="submit"><?php echo e(__('Submit')); ?></button>
          </form>
        </div>
        <div class="col-lg-7 px-4 px-xl-5 py-5">
           <div class="row">
           <?php if($additional->site_address_display == 1): ?>
           <div class="col-md-6 mb-grid-gutter">
           <div class="card">
            <div class="card-body text-center"><i class="dwg-location h3 mt-2 mb-4 text-primary"></i>
              <h3 class="h6 mb-2"><?php echo e(__('Office Address')); ?></h3>
              <p class="font-size-sm text-muted"><?php echo e($allsettings->office_address); ?></p>
             </div>
            </div>
            </div>
            <?php endif; ?>
            <?php if($additional->site_email_display == 1): ?>
            <div class="col-md-6 mb-grid-gutter"><a class="card" href="mailto:<?php echo e($allsettings->office_email); ?>">
          <div class="card-body text-center"><i class="dwg-mail h3 mt-2 mb-4 text-primary"></i>
              <h3 class="h6 mb-3"><?php echo e(__('Email')); ?></h3>
              <p class="font-size-sm text-muted"><?php echo e($allsettings->office_email); ?></p>
             </div>
            </a>
          </div>
          <?php endif; ?>
          <?php if($additional->site_phone_display == 1): ?>
          <div class="col-md-6 mb-grid-gutter"><a class="card" href="tel:<?php echo e($allsettings->office_phone); ?>">
            <div class="card-body text-center"><i class="dwg-phone h3 mt-2 mb-4 text-primary"></i>
              <h3 class="h6 mb-2"><?php echo e(__('Phone Number')); ?></h3>
              <p class="font-size-sm text-muted"><?php echo e($allsettings->office_phone); ?></p>
            </div></a>
            </div>
            <?php endif; ?>
          </div>
        </div>
        </div>
        <?php if(in_array('contact',$bottom_ads)): ?>
        <div class="row">
          <div class="col-lg-12 mt-2 mb-2" align="center">
             <?php echo html_entity_decode($addition_settings->bottom_ads); ?>
          </div>
       </div>   
       <?php endif; ?>
    </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php else: ?>
<?php echo $__env->make('503', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?><?php /**PATH /home/u596847059/domains/digizon.online/public_html/resources/views/contact.blade.php ENDPATH**/ ?>